package main

import (
	"aoc/util"
	"bufio"
	"fmt"
	"io"
	"sort"
	"strconv"
	"strings"
)

func main() {
	i := parseInput()
	s := newState(len(i), len(i[0]))
	for x := 0; x < len(i); x++ {
		for y := 0; y < len(i[x]); y++ {
			s.observeCell(x, y, i)
		}
	}
	fmt.Println("Result: ", s.result())
}

type input [][]int

type state struct {
	lastBasinId int
	basinSize   map[int]int
	markedCells [][]int
}

func newState(x, y int) *state {
	var s state
	s.basinSize = make(map[int]int)
	var cellsBackingBuffer = make([]int, x*y)
	for i := 0; i < x; i++ {
		s.markedCells = append(s.markedCells, cellsBackingBuffer[:y])
		cellsBackingBuffer = cellsBackingBuffer[y:]
	}
	return &s
}

func (s *state) observeCell(x, y int, grid input) {
	height := grid[x][y]

	if height == 9 {
		return
	}

	basinId := s.markedCells[x][y]
	if basinId != 0 {
		return
	}

	// generate a basin id
	s.lastBasinId++
	basinId = s.lastBasinId

	// flood-fill
	s.fillCell(x, y, basinId, grid)
}

func (s *state) fillCell(x, y, value int, grid input) {
	height := grid[x][y]
	if height == 9 {
		return
	}
	if s.markedCells[x][y] != 0 {
		return
	}

	s.markedCells[x][y] = value
	s.basinSize[value]++

	// fill all neighbors
	if x != 0 {
		s.fillCell(x-1, y, value, grid)
	}
	if x != len(grid)-1 {
		s.fillCell(x+1, y, value, grid)
	}
	if y != 0 {
		s.fillCell(x, y-1, value, grid)
	}
	if y != len(grid[x])-1 {
		s.fillCell(x, y+1, value, grid)
	}
}

func (s *state) result() int {
	// take the product of the size of the
	// three largest basins
	var results []int

	for _, size := range s.basinSize {
		results = append(results, size)
	}
	sort.Sort(sort.Reverse(sort.IntSlice(results)))

	result := results[0]
	result *= results[1]
	result *= results[2]

	return result
}

func parseInput() (retval input) {
	inputFile := util.OpenInputFile()
	defer inputFile.Close()

	r := bufio.NewReader(inputFile)
	for {
		line, err := r.ReadString('\n')
		if err != nil && err != io.EOF {
			util.ExitWithError("error reading input line in file: %s", err)
		}
		isEOF := err == io.EOF
		line = strings.TrimSpace(line)
		if line == "" && isEOF {
			// trailing blank okay
			break
		}

		var row []int
		for _, numStr := range line {
			n, err := strconv.Atoi(string(numStr))
			if err != nil {
				util.ExitWithError("unexpected input cell %q: %s", numStr, err)
			}
			row = append(row, n)
		}
		retval = append(retval, row)

		if isEOF {
			// fin!
			break
		}
	}
	return
}
