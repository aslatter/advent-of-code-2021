package main

import (
	"bufio"
	"fmt"
	"io"
	"strconv"
	"strings"

	"aoc/util"
)

func main() {
	file := util.OpenInputFile()
	defer file.Close()

	nums, err := readNums(file)
	if err != nil {
		util.ExitWithError("error parsing numbers from file: ", err)
	}

	fmt.Println("Result: ", result(nums))
}

func result(input numCollection) int {
	// the problem description refers to the msb as
	// 'bit 1', but in this program we number bits starting
	// with zero and starting with the lsb. So when the problem
	// description says 'start with bit 1 and go up' we must start
	// with bit 'bitLen - 1' and go down.
	origPos := input.bitLen - 1
	ox, co := divide(input, origPos)

	oxPos := origPos - 1
	for len(ox.nums) > 1 {
		ox, _ = divide(ox, oxPos)
		oxPos--
	}

	coPos := origPos - 1
	for len(co.nums) > 1 {
		_, co = divide(co, coPos)
		coPos--
	}

	return int(ox.nums[0]) * int(co.nums[0])
}

func divide(source numCollection, pos int) (ox numCollection, co numCollection) {
	if pos < 0 || pos >= source.bitLen {
		panic(fmt.Sprintf("illegal divide position: %d", pos))
	}
	ox.bitLen = source.bitLen
	co.bitLen = source.bitLen

	// first, we assume that '1' is the most popular, and put all 1s in the ox
	// collection and all zeros in the co collection, but then we also check at
	// the end to see if that assumption was correct.
	for _, n := range source.nums {
		if n&(0b1<<pos) != 0 {
			ox.nums = append(ox.nums, n)
		} else {
			co.nums = append(co.nums, n)
		}
	}

	if len(co.nums) > len(ox.nums) {
		ox, co = co, ox
	}

	return
}

type numCollection struct {
	nums   []uint64
	bitLen int
}

func readNums(r io.Reader) (numCollection, error) {
	var z numCollection
	var result numCollection
	br := bufio.NewReader(r)
	for {
		n, l, err := nextNumber(br)
		if err != nil && err != io.EOF {
			return z, err
		}
		if n == nil && err == io.EOF {
			// okay
			return result, nil
		}
		if n == nil {
			return z, fmt.Errorf("unexpected nil value from read")
		}

		result.bitLen = l
		result.nums = append(result.nums, *n)
		if err == io.EOF {
			return result, nil
		}
	}
}

func nextNumber(r *bufio.Reader) (*uint64, int, error) {
	var line string

	var err error

	for line == "" && err != io.EOF {
		line, err = r.ReadString('\n')
		if err != nil && err != io.EOF {
			return nil, 0, err
		}
		line = strings.TrimSpace(line)
	}

	if line == "" && err == io.EOF {
		return nil, 0, err
	}

	// preserve so we can pass EOF back up
	ioErr := err

	num, err := strconv.ParseUint(line, 2, 64)
	if err != nil {
		return nil, 0, err
	}

	return &num, len(line), ioErr
}
