package main

import (
	"flag"
	"fmt"
	"io"
	"strconv"
	"strings"

	"aoc/util"
)

func main() {
	flag.Parse()

	inputFile := util.OpenInputFile()
	defer inputFile.Close()

	b, err := io.ReadAll(inputFile)
	if err != nil {
		util.ExitWithError("reading file: %s", err)
	}

	var result int
	c := newCache()

	// because there is no interaction between fishes
	// I can calculate the impact of a single input fish
	// on the ecosystem in turn, and sum up the results

	pieces := strings.Split(string(b), ",")
	for _, numStr := range pieces {
		numStr = strings.TrimSpace(numStr)
		num, err := strconv.Atoi(numStr)
		if err != nil {
			util.ExitWithError("error parsing number in input: %q: %s", numStr, err)
		}

		result += c.impact(num, days)
	}

	fmt.Println("Result: ", result)
}

var days = 80

func init() {
	flag.IntVar(&days, "days", days, "number of days to run simulation for")
}

// we cache intermediate solutions in a map.
// the index is the arguments passed to the
// 'impact' function.
type cache map[[2]int]int

func newCache() cache {
	return make(cache)
}

// remainder: number of days remaining
func (c cache) impact(timerVal int, remainder int) int {
	if remainder == 0 {
		// base case of recursion - the input fish
		// itself contributes.
		return 1
	}

	// maybe there is some math trick we can do to make this cheaper,
	// but this is probably cheap enough

	// also I'm not sure if the cache really buys us much in
	// the recursive case.

	// (and doing a bit of testing, the input size is small
	// enough the cache doesn't really matter at all. oh well.)

	// (and after trying part 2, the cache does seem to help here!)

	// check if we've solved this problem before - if so,
	// return it.
	index := [2]int{timerVal, remainder}
	n, ok := c[index]
	if ok {
		return n
	}

	// otherwise, recurse
	if timerVal == 0 {
		// breed! create a new fish, and reset timer
		// of current fish.
		n = c.impact(6, remainder-1) + c.impact(8, remainder-1)
	} else {
		n = c.impact(timerVal-1, remainder-1)
	}

	// insert into cache before returning
	c[index] = n
	return n
}
