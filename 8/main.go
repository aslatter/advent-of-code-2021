package main

import (
	"aoc/util"
	"bufio"
	"fmt"
	"io"
	"sort"
	"strconv"
	"strings"
)

func main() {
	i := parseInput()
	var s state
	for _, event := range i {
		s.obsever(event)
	}
	fmt.Println("Result: ", s.sum)
}

type inputEvent struct {
	signals [10]string
	outputs [4]string
}

type input []inputEvent

type state struct {
	sum int
}

func (s *state) obsever(e inputEvent) {
	solvedDigits := make(map[int]string)

	// signals of length six: 0, 6, 9
	sixes := make(map[string]struct{})

	// signals of length five: 2, 3, 5
	fives := make(map[string]struct{})

	for _, s := range e.signals {
		switch len(s) {
		case 2:
			solvedDigits[1] = s
		case 3:
			solvedDigits[7] = s
		case 4:
			solvedDigits[4] = s
		case 7:
			solvedDigits[8] = s
		case 5:
			fives[s] = struct{}{}
		case 6:
			sixes[s] = struct{}{}
		}
	}

	// find '3' in fives group - it
	// is the only one with all segments
	// in '1'
outer1:
	for s := range fives {
		for _, r := range solvedDigits[1] {
			if !strings.ContainsRune(s, r) {
				continue outer1
			}
		}
		// got it!
		solvedDigits[3] = s
		delete(fives, s)
		break
	}

	// From sixes group pick out 9 (using 4)
outer2:
	for s := range sixes {
		for _, r := range solvedDigits[4] {
			if !strings.ContainsRune(s, r) {
				continue outer2
			}
		}
		// got it!
		solvedDigits[9] = s
		delete(sixes, s)
	}

	// From sixes group pick out 0 (using 1)
outer3:
	for s := range sixes {
		for _, r := range solvedDigits[1] {
			if !strings.ContainsRune(s, r) {
				continue outer3
			}
		}
		// got it!
		solvedDigits[0] = s
		delete(sixes, s)
	}

	// sixes only has 6 left
	for s := range sixes {
		solvedDigits[6] = s
	}

	// fives has 2 and 5 left in it.
	// '5' overlaps 3 segments with '4' ('2' does not)
	for s := range fives {
		var overlap int
		for _, r := range solvedDigits[4] {
			if strings.ContainsRune(s, r) {
				overlap++
			}
		}
		if overlap == 3 {
			solvedDigits[5] = s
			delete(fives, s)
		}
	}

	// only remaining in fives is 2
	for s := range fives {
		solvedDigits[2] = s
	}

	// invert digits -> segment map
	solvedSegments := make(map[string]int)
	for k, v := range solvedDigits {
		solvedSegments[v] = k
	}

	// build output
	var numStr string
	for _, out := range e.outputs {
		numStr += fmt.Sprint(solvedSegments[out])
	}

	result, err := strconv.Atoi(numStr)
	if err != nil {
		panic(err)
	}

	s.sum += result
}

/*

  0:      1:      2:      3:      4:
 aaaa    ....    aaaa    aaaa    ....
b    c  .    c  .    c  .    c  b    c
b    c  .    c  .    c  .    c  b    c
 ....    ....    dddd    dddd    dddd
e    f  .    f  e    .  .    f  .    f
e    f  .    f  e    .  .    f  .    f
 gggg    ....    gggg    gggg    ....

  5:      6:      7:      8:      9:
 aaaa    aaaa    aaaa    aaaa    aaaa
b    .  b    .  .    c  b    c  b    c
b    .  b    .  .    c  b    c  b    c
 dddd    dddd    ....    dddd    dddd
.    f  e    f  .    f  e    f  .    f
.    f  e    f  .    f  e    f  .    f
 gggg    gggg    ....    gggg    gggg

*/

func parseInput() (retval input) {
	inputFile := util.OpenInputFile()
	defer inputFile.Close()
	r := bufio.NewReader(inputFile)
	for {
		line, err := r.ReadString('\n')
		if err != nil && err != io.EOF {
			panic(err)
		}

		isEOF := err == io.EOF

		line = strings.TrimSpace(line)
		if line == "" && isEOF {
			// okay
			break
		}

		pieces := strings.SplitN(line, "|", 2)
		if len(pieces) != 2 {
			panic(fmt.Sprintf("Expected 2 pipe-delimitted pieces, got %q", line))
		}

		// canonicalize the numerals
		signals := strings.Fields(pieces[0])
		outputs := strings.Fields(pieces[1])

		for i, s := range signals {
			signals[i] = canonicalizeSignal(s)
		}
		for i, s := range outputs {
			outputs[i] = canonicalizeSignal(s)
		}

		var e inputEvent
		copy(e.signals[:], signals)
		copy(e.outputs[:], outputs)
		retval = append(retval, e)
	}
	return
}

func canonicalizeSignal(s string) string {
	rs := []rune(s)
	sort.Slice(rs, func(i, j int) bool {
		return rs[i] < rs[j]
	})
	return string(rs)
}
