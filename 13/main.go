package main

import (
	"aoc/util"
	"bufio"
	"fmt"
	"io"
	"strconv"
	"strings"
)

const xRange = 1500
const yRange = 1500

func main() {
	state := parseInput()

	// perform folds
	for _, aFold := range state.instructions {
		doAFold(&state, aFold)
	}

	// find bounds of remaining dots
	// (we could cache this from instructions but I'm
	// too lazy)
	var maxX int
	var maxY int
	for x := 0; x < xRange; x++ {
		for y := 0; y < yRange; y++ {
			if state.dots[x][y] {
				if x > maxX {
					maxX = x
				}
				if y > maxY {
					maxY = y
				}
			}
		}
	}

	// print image
	for y := 0; y <= maxY; y++ {
		for x := 0; x <= maxX; x++ {
			image := " "
			if state.dots[x][y] {
				image = "#"
			}
			fmt.Print(image)
		}
		fmt.Println()
	}
}

type input struct {
	dots         [][]bool
	instructions []instruction
}

func doAFold(state *input, fold instruction) {
	if fold.isXDirection {
		// find dots to mirror to the left
		for xpos := fold.pos + 1; xpos < xRange; xpos++ {
			for ypos := 0; ypos < yRange; ypos++ {
				if state.dots[xpos][ypos] {
					reflectPos := 2*fold.pos - xpos
					state.dots[reflectPos][ypos] = true
					state.dots[xpos][ypos] = false
				}
			}
		}
		return
	}

	// find dots to mirror up
	for ypos := fold.pos + 1; ypos < yRange; ypos++ {
		for xpos := 0; xpos < xRange; xpos++ {
			if state.dots[xpos][ypos] {
				reflectPos := 2*fold.pos - ypos
				state.dots[xpos][reflectPos] = true
				state.dots[xpos][ypos] = false
			}
		}
	}

}

type instruction struct {
	pos          int
	isXDirection bool
}

func parseInput() input {
	inputFile := util.OpenInputFile()
	defer inputFile.Close()

	r := bufio.NewReader(inputFile)

	var retVal input
	dotsMemory := make([]bool, xRange*yRange)
	for i := 0; i < xRange; i++ {
		retVal.dots = append(retVal.dots, dotsMemory[:yRange])
		dotsMemory = dotsMemory[yRange:]
	}

	var isEOF bool
	// parse dots
	for {
		text, err := r.ReadString('\n')
		if err != nil && err != io.EOF {
			panic(err)
		}
		isEOF = err == io.EOF
		text = strings.TrimSpace(text)
		if text == "" {
			break
		}

		// form of dot-line is 'num,num'
		pieces := strings.Split(text, ",")
		if len(pieces) != 2 {
			panic(fmt.Sprintf("unexpected position-line: %q", text))
		}
		xStr := pieces[0]
		yStr := pieces[1]

		x, err := strconv.Atoi(xStr)
		if err != nil {
			panic(fmt.Sprintf("unexpected position-line: %q: %s", text, err))
		}

		y, err := strconv.Atoi(yStr)
		if err != nil {
			panic(fmt.Sprintf("unexpected position-line: %q: %s", text, err))
		}

		retVal.dots[x][y] = true

		if isEOF {
			break
		}
	}

	// parse instructions
	for {
		text, err := r.ReadString('\n')
		if err != nil && err != io.EOF {
			panic(err)
		}
		isEOF = err == io.EOF
		text = strings.TrimSpace(text)
		if text == "" && isEOF {
			break
		}

		origText := text

		if !strings.HasPrefix(text, "fold along ") {
			panic(fmt.Sprintf("unexpected instruction: %q", origText))
		}
		text = strings.TrimPrefix(text, "fold along ")
		dirStr, posStr, ok := strings.Cut(text, "=")
		if !ok {
			panic(fmt.Sprintf("unexpected instruction: %q", origText))
		}

		var isX bool

		switch dirStr {
		case "x":
			isX = true
		case "y":
		default:
			panic(fmt.Sprintf("unexpected instruction: %q (dirStr: %q)", origText, dirStr))
		}

		pos, err := strconv.Atoi(posStr)
		if err != nil {
			panic(fmt.Sprintf("unexpected instruction: %q: %s", origText, err))
		}

		retVal.instructions = append(retVal.instructions, instruction{
			isXDirection: isX,
			pos:          pos,
		})
	}

	return retVal
}
