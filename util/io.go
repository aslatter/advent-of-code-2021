package util

import (
	"flag"
	"fmt"
	"io"
	"os"
)

// OpenInputFile opens the first argument as a file or
// exits with failure.
func OpenInputFile() io.ReadCloser {
	args := os.Args[1:]
	if flag.Parsed() {
		args = flag.Args()
	}
	if len(args) < 1 {
		fmt.Fprintln(os.Stderr, "missing argument: input file")
		os.Exit(1)
	}

	fileName := args[0]
	file, err := os.Open(fileName)
	if err != nil {
		fmt.Fprintf(os.Stderr, "unable to open file %q: %s\n", fileName, err)
		os.Exit(1)
	}
	return file
}

// ExitWithError prints a message to stderr and exits the
// program with a failure exit-code.
func ExitWithError(format string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, format, args...)
	os.Exit(1)
}
