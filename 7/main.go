package main

import (
	"aoc/util"
	"fmt"
	"io"
	"sort"
	"strconv"
	"strings"
)

func main() {
	nums := parseInput()
	sort.Ints(nums)

	// jank newton's method

	// our initial guess will be the median
	// (which happens to be correct for pt 1 ... tricksy!)
	guess := nums[len(nums)/2]

	cost := score(nums, guess)

	fmt.Printf("guess:\t%d\ncost:\t%d\n", guess, cost)

	// iterate until the next iteration was the same as the
	// previous

	for {
		d := deriv(nums, guess)
		dd := deriv_deriv(nums, guess)

		nextGuess := int((float64(guess) - d/dd) + 0.5)
		nextScore := score(nums, nextGuess)

		fmt.Printf("next guess:\t%d\nnext cost:\t%d\n", nextGuess, nextScore)

		if guess == nextGuess {
			break
		}
		guess = nextGuess
	}
}

func parseInput() []int {
	inputFile := util.OpenInputFile()
	defer inputFile.Close()

	b, err := io.ReadAll(inputFile)
	if err != nil {
		panic(err)
	}
	pieces := strings.Split(string(b), ",")

	var retVal []int
	for _, numStr := range pieces {
		numStr = strings.TrimSpace(numStr)
		num, err := strconv.Atoi(numStr)
		if err != nil {
			panic(err)
		}
		retVal = append(retVal, num)
	}

	return retVal
}

// score returns the cost of a choice in position
func score(nums []int, pos int) (retval int) {
	for _, n := range nums {
		retval += score_diff_pos(n, pos)
	}
	return
}

func score_diff_pos(start int, end int) int {
	// there is probably a closed-form for this
	diff := start - end
	if diff < 0 {
		diff = -diff
	}
	var s int
	i := 1
	for diff > 0 {
		s += i
		i++
		diff--
	}

	return s
}

// deriv approximates the first-derivative of score.
func deriv(nums []int, pos int) float64 {
	left := score(nums, pos-1)
	right := score(nums, pos+1)
	return float64(right-left) / 2
}

// deriv_deriv approximates the seconds-derivative of score.
func deriv_deriv(nums []int, pos int) float64 {
	// we're double-calculating scores, but I'm
	// too lazy to fix that here.

	left := deriv(nums, pos-1)
	right := deriv(nums, pos+1)
	return (right - left) / 2
}
