package main

import (
	"aoc/util"
	"bufio"
	"fmt"
	"io"
	"strings"
)

const rounds = 40

func main() {
	input := parseInput()

	// build map of all rules
	rules := make(map[[2]byte]byte)
	for _, aRule := range input.rules {
		rules[aRule.from] = aRule.to
	}

	state := newState()
	state.rules = rules

	popCount := make(score)
	// edge-case - score the first byte in the template
	popCount[input.template[0]]++
	// score each pair in turn
	for i := 0; i < len(input.template)-1; i++ {
		pair := [2]byte{input.template[i], input.template[i+1]}
		for k, v := range state.scorePair(pair, rounds) {
			popCount[k] += v
		}
	}

	var maxCount int
	var minCount int
	for _, aCount := range popCount {
		if aCount > maxCount {
			maxCount = aCount
		}
		if minCount == 0 || aCount < minCount {
			minCount = aCount
		}
	}

	fmt.Printf("min: %d\nmax: %d\ndiff: %d\n", minCount, maxCount, maxCount-minCount)
}

type state struct {
	rules      map[[2]byte]byte
	scoreCache map[stepInput]score
}

func newState() state {
	return state{
		rules:      make(map[[2]byte]byte),
		scoreCache: make(map[stepInput]score),
	}
}

func (s *state) scorePair(pair [2]byte, remainingRounds int) score {
	if remainingRounds < 1 {
		// base-case - score the right-most character
		retval := make(score)
		retval[pair[1]]++
		return retval
	}

	// have we seen this before?
	cacheKey := stepInput{
		pair:            pair,
		remainingRounds: remainingRounds,
	}
	cachedScore, ok := s.scoreCache[cacheKey]
	if ok {
		return cachedScore
	}

	mid, ok := s.rules[pair]
	if !ok {
		// not really worth caching
		retval := make(score)
		retval[pair[1]]++
		return retval
	}

	leftPair := [2]byte{pair[0], mid}
	rightPair := [2]byte{mid, pair[1]}

	retval := make(score)

	for k, v := range s.scorePair(leftPair, remainingRounds-1) {
		retval[k] = v
	}
	for k, v := range s.scorePair(rightPair, remainingRounds-1) {
		retval[k] += v
	}

	s.scoreCache[cacheKey] = retval

	return retval
}

type stepInput struct {
	pair            [2]byte
	remainingRounds int
}
type score map[byte]int

type input struct {
	template []byte
	rules    []rule
}

type rule struct {
	from [2]byte
	to   byte
}

func parseInput() (retval input) {
	file := util.OpenInputFile()
	defer file.Close()

	r := bufio.NewReader(file)

	// template is first line
	template, err := r.ReadString('\n')
	if err != nil {
		panic(err)
	}
	retval.template = []byte(strings.TrimSpace(template))

	// consume blank line
	_, err = r.ReadString('\n')
	if err != nil {
		panic(err)
	}

	// remaining lines are rules
	for {
		text, err := r.ReadString('\n')
		if err != nil && err != io.EOF {
			panic(err)
		}
		isEOF := err == io.EOF

		text = strings.TrimSpace(text)
		if text == "" && isEOF {
			// okay to be done
			break
		}

		from, to, ok := strings.Cut(text, " -> ")
		if !ok {
			panic(fmt.Sprintf("unexpected rule: %q", text))
		}

		fromBytes := []byte(from)
		if len(fromBytes) != 2 {
			panic(fmt.Sprintf("unexpected rule: %q", text))
		}

		toBytes := []byte(to)
		if len(toBytes) != 1 {
			panic(fmt.Sprintf("unexpected rule: %q", text))
		}

		var newRule rule
		newRule.to = toBytes[0]
		newRule.from[0] = fromBytes[0]
		newRule.from[1] = fromBytes[1]

		retval.rules = append(retval.rules, newRule)

		if isEOF {
			break
		}
	}

	return
}
