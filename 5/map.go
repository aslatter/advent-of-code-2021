package main

import (
	"fmt"
	"strconv"
	"strings"
)

const dangerThreshold = 2

type pos [2]int
type vent [2]pos

type state struct {
	totals         [1001][1001]int
	aboveThreshold map[pos]struct{}
}

func newState() *state {
	var s state
	s.aboveThreshold = make(map[pos]struct{})
	return &s
}

func (s *state) observe(v vent) {
	x0 := v[0][0]
	x1 := v[1][0]

	y0 := v[0][1]
	y1 := v[1][1]

	// normalize to x0 <= x1
	if x1 < x0 {
		x0, x1 = x1, x0
		y0, y1 = y1, y0
	}

	// if x0 == x1, normalize to y0 <= y1
	if x0 == x1 && y1 < y0 {
		y0, y1 = y1, y0
	}

	switch {
	case x0 == x1:
		x := x0
		for y := y0; y <= y1; y++ {
			s.increment(x, y)
		}
	case y0 == y1:
		y := y0
		for x := x0; x <= x1; x++ {
			s.increment(x, y)
		}
	case y0 > y1:
		x, y := x0, y0
		for {
			if !(x <= x1 && y >= y1) {
				break
			}
			s.increment(x, y)
			x++
			y--
		}
	default:
		x, y := x0, y0
		for {
			if !(x <= x1 && y <= y1) {
				break
			}
			s.increment(x, y)
			x++
			y++
		}
	}
}

func (s *state) increment(x int, y int) {
	s.totals[x][y]++
	if s.totals[x][y] < dangerThreshold {
		return
	}
	s.aboveThreshold[[2]int{x, y}] = struct{}{}
}

func (s *state) result() int {
	return len(s.aboveThreshold)
}

func parseVent(s string) (v vent, err error) {
	// "x1,y1 -> x2,y2"
	pieces := strings.Fields(s)
	if len(pieces) != 3 {
		return v, fmt.Errorf("unable to parse input line: %q", s)
	}
	if pieces[1] != "->" {
		return v, fmt.Errorf("unable to parse input line: %q", s)
	}
	pieces = []string{pieces[0], pieces[2]}
	for i := range pieces {
		pairString := pieces[i]
		pairSplit := strings.Split(pairString, ",")
		if len(pairSplit) != 2 {
			return v, fmt.Errorf("unable to parse input line: %q", s)
		}
		for j, numString := range pairSplit {
			num, err := strconv.Atoi(numString)
			if err != nil {
				return v, fmt.Errorf("unable to parse input line: %q: %s", s, err)
			}
			v[i][j] = num
		}
	}

	return v, nil
}
