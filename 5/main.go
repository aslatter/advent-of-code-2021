package main

import (
	"aoc/util"
	"bufio"
	"fmt"
	"io"
	"strings"
)

func main() {
	inputFile := util.OpenInputFile()
	defer inputFile.Close()

	r := bufio.NewReader(inputFile)
	s := newState()
	for {
		line, err := r.ReadString('\n')
		if err != nil && err != io.EOF {
			util.ExitWithError("error reading line: %s", err)
		}
		line = strings.TrimSpace(line)
		if line == "" && err == io.EOF {
			// allow trailing line in file
			break
		}

		isEOF := err == io.EOF

		vent, err := parseVent(line)
		if err != nil {
			util.ExitWithError("unable to parse vent from line: %s", err)
		}

		s.observe(vent)

		if isEOF {
			break
		}
	}

	fmt.Println("Result: ", s.result())
}
