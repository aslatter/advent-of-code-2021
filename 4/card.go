package main

import (
	"fmt"
	"strconv"
	"strings"
)

type cardCollection map[*card]struct{}

func (col cardCollection) observe(called int) {
	for c := range col {
		c.observe(called)
	}
}

func (col cardCollection) winningCards() []*card {
	var retVal []*card
	for c := range col {
		if c.isWinner() {
			retVal = append(retVal, c)
		}
	}
	return retVal
}

func (col cardCollection) remove(c *card) {
	delete(col, c)
}

type card struct {
	nums            [5][5]int
	covered         [5][5]bool
	coveredInRow    [5]int
	coveredInColumn [5]int
}

func (c *card) observe(called int) {
	for i := 0; i < 5; i++ {
		for j := 0; j < 5; j++ {
			if c.nums[i][j] == called {
				if !c.covered[i][j] {
					c.covered[i][j] = true
					c.coveredInColumn[i]++
					c.coveredInRow[j]++
				}
				// assumption: a number only appears once per card
				return
			}
		}
	}
}

func (c *card) isWinner() bool {
	for i := 0; i < 5; i++ {
		if c.coveredInRow[i] >= 5 || c.coveredInColumn[i] >= 5 {
			return true
		}
	}
	return false
}

func (c *card) unmarkedSum() int {
	var sum int
	for i := 0; i < 5; i++ {
		for j := 0; j < 5; j++ {
			if !c.covered[i][j] {
				sum += c.nums[i][j]
			}
		}
	}
	return sum
}

func parseCard(s []string) (*card, error) {
	if len(s) != 5 {
		return nil, fmt.Errorf("invalid number of lines: %d", len(s))
	}

	var c card
	// each line contains 5 space-delimited (decimal) integers
	// I'm probably transposing the input here, but the problem
	// statement is invariant to that.
	for i := 0; i < 5; i++ {
		pieces := strings.Fields(s[i])
		if len(pieces) != 5 {
			return nil, fmt.Errorf("invalid input for line %d: %q", i, s[i])
		}
		for j := 0; j < 5; j++ {
			n, err := strconv.Atoi(pieces[j])
			if err != nil {
				return nil, fmt.Errorf("invalid input for line %d, column %d: %q", i, j, pieces[j])
			}
			c.nums[i][j] = n
		}
	}

	return &c, nil
}
