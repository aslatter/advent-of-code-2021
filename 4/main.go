package main

import (
	"aoc/util"
	"bufio"
	"fmt"
	"io"
	"strconv"
	"strings"
)

func main() {
	input, err := parseInput()
	if err != nil {
		util.ExitWithError("parsing input: %s", err)
	}

	cards := input.cards

	// observe each callout, check for winner
	var winningScore int

	for _, called := range input.toCall {
		cards.observe(called)
		winners := cards.winningCards()
		if len(winners) > 0 {
			// here I assume it doesn't matter which card from the winning set I choose,
			// because the puzzle does not supply me with a tie-breaker, the final set
			// of winners must be unary.
			winner := winners[0]
			winningScore = called * winner.unmarkedSum()
		}
		// do not allow a card to win more than once
		for _, c := range winners {
			cards.remove(c)
		}
	}

	fmt.Println("Result: ", winningScore)
}

type inputs struct {
	toCall []int
	cards  cardCollection
}

func parseInput() (*inputs, error) {
	inputFile := util.OpenInputFile()
	defer inputFile.Close()

	r := bufio.NewReader(inputFile)

	var retVal inputs
	retVal.cards = make(cardCollection)

	// first line - numbers to call, decimal, comma delimited
	calledString, err := r.ReadString('\n')
	if err != nil {
		return nil, err
	}
	calledString = strings.TrimSpace(calledString)
	for _, calledNumString := range strings.Split(calledString, ",") {
		calledNum, err := strconv.Atoi(calledNumString)
		if err != nil {
			return nil, fmt.Errorf("error parsing called number: %s", err)
		}
		retVal.toCall = append(retVal.toCall, calledNum)
	}

	// followed by a null line
	_, err = r.ReadString('\n')
	if err != nil {
		return nil, err
	}

	// followed by cards
	for {
		var err error
		var lines []string
		for {
			var line string
			line, err = r.ReadString('\n')
			if err != nil && err != io.EOF {
				return nil, err
			}
			line = strings.TrimSpace(line)
			if line == "" {
				// done with one card
				break
			}
			lines = append(lines, line)
			if err == io.EOF {
				// done with one card
			}
		}

		ioErr := err // preserve so we can check for EOF

		card, err := parseCard(lines)
		if err != nil {
			return nil, fmt.Errorf("parsing card: %s", err)
		}
		retVal.cards[card] = struct{}{}

		if ioErr == io.EOF {
			break
		}
	}

	return &retVal, nil
}
