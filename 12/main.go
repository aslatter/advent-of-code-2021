package main

import (
	"aoc/util"
	"bufio"
	"fmt"
	"io"
	"strings"
)

func main() {
	c := parseInput()
	var s state
	s.c = c
	paths := countPathsThrough(&s, "start")
	fmt.Println("Result: ", paths)
}

func countPathsThrough(old *state, room string) int {
	if room == "end" {
		// increment by 1 if we reach the end
		return 1
	}
	// copy state to not affect caller
	s := old.clone()

	// if this is our second time in, flag us
	// as not being allowed to do this again
	if s.isVisited(room) {
		s.seenSmallRoomTwice = true
	}

	// mark current room as visited
	s.markVisited(room)

	// count paths for connections
	var paths int
	for _, outRoom := range s.c.connections(room) {
		if outRoom == "start" {
			// never go back
			continue
		}
		if s.isVisited(outRoom) && s.seenSmallRoomTwice {
			continue
		}
		paths += countPathsThrough(s, outRoom)
	}
	return paths
}

type state struct {
	c                  connections
	visited            map[string]bool
	seenSmallRoomTwice bool
}

func (s *state) clone() *state {
	var s2 state
	s2.c = s.c
	for k, v := range s.visited {
		if v {
			s2.markVisited(k)
		}
	}
	s2.seenSmallRoomTwice = s.seenSmallRoomTwice
	return &s2
}

func (s *state) markVisited(room string) {
	// don't flag large rooms as visited
	if strings.ToUpper(room) == room {
		return
	}
	if s.visited == nil {
		s.visited = make(map[string]bool)
	}
	s.visited[room] = true
}

func (s *state) isVisited(room string) bool {
	return s.visited[room]
}

type connections map[string][]string

func newConnections() connections {
	return make(connections)
}

func (c connections) addConnection(a string, b string) {
	c[a] = append(c[a], b)
	c[b] = append(c[b], a)
}

func (c connections) connections(a string) []string {
	return c[a]
}

func parseInput() connections {
	inputFile := util.OpenInputFile()
	defer inputFile.Close()
	r := bufio.NewReader(inputFile)

	c := newConnections()

	for {
		line, err := r.ReadString('\n')
		if err != nil && err != io.EOF {
			panic(err)
		}
		isEOF := err == io.EOF
		line = strings.TrimSpace(line)
		if line == "" && isEOF {
			// okay to stop
			break
		}

		// parse connection.
		// form: room-room
		pieces := strings.Split(line, "-")
		if len(pieces) != 2 {
			panic(fmt.Sprintf("unexpected input string: %q", line))
		}

		// add to result
		c.addConnection(pieces[0], pieces[1])

		if isEOF {
			break
		}
	}

	return c
}
