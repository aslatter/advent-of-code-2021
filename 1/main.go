package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func main() {
	args := os.Args
	if len(args) < 2 {
		fmt.Fprint(os.Stderr, "missing required argument - input file")
		os.Exit(1)
	}

	inputFileName := args[1]
	input, err := os.Open(inputFileName)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error opening file %q: %s", inputFileName, err)
	}
	defer input.Close()

	r := bufio.NewReader(input)
	var s sumState
	for {
		next := nextNumber(r)
		if !next.skip {
			s.observe(next.num)
		}
		if next.isDone {
			break
		}
	}
	reportCount(s.increases)
}

func reportCount(count int) {
	fmt.Println("number of increases: ", count)
}

type sumState struct {
	increases int
	seen      int
	sumOld    int
	counts    [3]int
}

func (s *sumState) observe(value int) {
	for i := 0; i < 3; i++ {
		s.counts[i] += value
	}
	s.seen++
	// rotate & check
	if s.seen >= 4 {
		if s.counts[0] > s.sumOld {
			s.increases++
		}
	}
	s.sumOld = s.counts[0]
	s.counts[0] = s.counts[1]
	s.counts[1] = s.counts[2]
	s.counts[2] = 0
}

type nextNum struct {
	num    int
	isDone bool
	skip   bool
}

func nextNumber(r *bufio.Reader) (retVal nextNum) {
	line, err := r.ReadString('\n')
	if err != nil && err != io.EOF {
		fmt.Fprintf(os.Stderr, "error while reading from file: %s", err)
		os.Exit(1)
	}

	retVal.isDone = err == io.EOF

	line = strings.TrimSuffix(line, "\n")

	if line == "" {
		retVal.skip = true
		return
	}

	num, err := strconv.Atoi(line)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error parsing line %q: %s", line, err)
		os.Exit(1)
	}

	retVal.num = num
	return
}
