package main

import (
	"aoc/util"
	"bufio"
	"fmt"
	"io"
	"sort"
	"strings"
)

func main() {
	var s state
	lines := parseInput()
	for _, line := range lines {
		s.observe(line)
	}
	fmt.Println("Result: ", s.result())
}

type state struct {
	scores []int
}

func (s *state) observe(line string) {
	var st stack
	for _, r := range line {
		if isOpen(r) {
			st.push(r)
			continue
		}
		expected := st.pop()
		if r == expected {
			continue
		}
		// invalid! done with line
		return
	}
	// line is valid but potentially incomplete.
	// drain the remainder of 'st' to get this
	// line's autocomplete score.
	var lineScore int
	for len(st) > 0 {
		remaining := st.pop()
		lineScore *= 5
		lineScore += score(remaining)
	}
	s.scores = append(s.scores, lineScore)
}

func (s *state) result() int {
	sort.Ints(s.scores)
	return s.scores[len(s.scores)/2]
}

type stack []rune

func (s *stack) push(x rune) {
	*s = append(*s, invert(x))
}

func (s *stack) pop() (r rune) {
	if len(*s) == 0 {
		panic("stack underflow")
	}
	r = (*s)[len(*s)-1]
	*s = (*s)[:len(*s)-1]
	return
}

func isOpen(r rune) bool {
	switch r {
	case '[', '{', '(', '<':
		return true
	default:
		return false
	}
}

func score(r rune) int {
	switch r {
	case ')':
		return 1
	case ']':
		return 2
	case '}':
		return 3
	case '>':
		return 4
	default:
		panic(fmt.Sprintf("unexpected input: %q", r))
	}
}

func invert(r rune) rune {
	switch r {
	case '[':
		return ']'
	case '{':
		return '}'
	case '(':
		return ')'
	case '<':
		return '>'
	default:
		panic(fmt.Sprintf("unexpected input: %q", r))
	}
}

func parseInput() []string {
	inputFile := util.OpenInputFile()
	defer inputFile.Close()

	var retval []string
	r := bufio.NewReader(inputFile)
	for {
		line, err := r.ReadString('\n')
		if err != nil && err != io.EOF {
			panic(err)
		}
		isEOF := err == io.EOF

		line = strings.TrimSpace(line)
		if line == "" && isEOF {
			// okay to be done
			break
		}

		retval = append(retval, line)
		if isEOF {
			break
		}
	}

	return retval
}
