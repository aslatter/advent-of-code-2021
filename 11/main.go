package main

import (
	"aoc/util"
	"bufio"
	"fmt"
	"io"
	"strconv"
	"strings"
)

func main() {
	grid := parseInput()
	var stepCount int
	var s state
	for !s.done {
		s.step(grid)
		stepCount++
	}
	fmt.Println("Result: ", stepCount)
}

type input [][]int

type state struct {
	done       bool
	flashCount int
}

func (s *state) step(grid input) {
	// reset flash-count (we want it to be per step for pt 2)
	s.flashCount = 0

	// increment all cells
	for x := 0; x < len(grid); x++ {
		for y := 0; y < len(grid[x]); y++ {
			grid[x][y]++
		}
	}

	// fire flashes
	for x := 0; x < len(grid); x++ {
		for y := 0; y < len(grid[x]); y++ {
			if grid[x][y] > 9 {
				s.fireFlash(grid, x, y)
			}
		}
	}

	// set the 'done' flag if all cells flashed this step
	if s.flashCount == len(grid)*len(grid[0]) {
		s.done = true
	}
}

func (s *state) fireFlash(grid input, x, y int) {
	// have we already fired this cell for this step?
	if grid[x][y] == 0 {
		return
	}

	// increment (we over-increment the starting cell, but this is
	// easier to implement and doesn't matter)
	grid[x][y]++

	// do we need to flash?
	if grid[x][y] < 10 {
		return
	}
	// go!
	grid[x][y] = 0
	s.flashCount++

	// direct neighbors
	if x > 0 {
		s.fireFlash(grid, x-1, y)
	}
	if x < len(grid)-1 {
		s.fireFlash(grid, x+1, y)
	}
	if y > 0 {
		s.fireFlash(grid, x, y-1)
	}
	if y < len(grid[x])-1 {
		s.fireFlash(grid, x, y+1)
	}

	// corners
	if x > 0 && y > 0 {
		s.fireFlash(grid, x-1, y-1)
	}
	if x > 0 && y < len(grid[x])-1 {
		s.fireFlash(grid, x-1, y+1)
	}
	if x < len(grid)-1 && y > 0 {
		s.fireFlash(grid, x+1, y-1)
	}
	if x < len(grid)-1 && y < len(grid[x])-1 {
		s.fireFlash(grid, x+1, y+1)
	}
}

func parseInput() (retval input) {
	inputFile := util.OpenInputFile()
	defer inputFile.Close()

	r := bufio.NewReader(inputFile)
	for {
		line, err := r.ReadString('\n')
		if err != nil && err != io.EOF {
			util.ExitWithError("error reading input line in file: %s", err)
		}
		isEOF := err == io.EOF
		line = strings.TrimSpace(line)
		if line == "" && isEOF {
			// trailing blank okay
			break
		}

		var row []int
		for _, numStr := range line {
			n, err := strconv.Atoi(string(numStr))
			if err != nil {
				util.ExitWithError("unexpected input cell %q: %s", numStr, err)
			}
			row = append(row, n)
		}
		retval = append(retval, row)

		if isEOF {
			// fin!
			break
		}
	}
	return
}
