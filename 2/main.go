package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func main() {
	args := os.Args
	if len(args) < 2 {
		fmt.Fprint(os.Stderr, "missing required argument - input file")
		os.Exit(1)
	}

	inputFileName := args[1]
	input, err := os.Open(inputFileName)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error opening file %q: %s", inputFileName, err)
	}
	defer input.Close()

	r := bufio.NewReader(input)

	var s state

	for {
		cmd, err := nextCommand(r)
		if err != nil && err != io.EOF {
			fmt.Fprintf(os.Stderr, "error parsing command: %s\n", err)
			os.Exit(1)
		}
		if cmd == nil && err == io.EOF {
			// okay
			break
		}
		if cmd == nil {
			fmt.Fprintln(os.Stderr, "unexpected nil command")
			os.Exit(1)
		}

		s.observe(*cmd)

		if err == io.EOF {
			break
		}
	}

	fmt.Println("Result: ", s.result())
}

type state struct {
	aim       int
	depth     int
	hPosition int
}

func (s *state) observe(cmd command) {
	switch cmd.direction {
	case up:
		s.aim -= cmd.quantity
	case down:
		s.aim += cmd.quantity
	case forward:
		s.hPosition += cmd.quantity
		s.depth += cmd.quantity * s.aim
	}
}

func (s *state) result() int {
	return s.depth * s.hPosition
}

type direction int

const (
	up      direction = iota
	down    direction = iota
	forward direction = iota
)

type command struct {
	direction direction
	quantity  int
}

func nextCommand(r *bufio.Reader) (*command, error) {
	var line string

	var err error

	for line == "" && err != io.EOF {
		line, err = r.ReadString('\n')
		if err != nil && err != io.EOF {
			return nil, err
		}
		line = strings.TrimSpace(line)
	}

	if line == "" && err == io.EOF {
		return nil, err
	}

	// preserve so we can pass EOF back up
	ioErr := err

	pieces := strings.SplitN(line, " ", 2)
	if len(pieces) != 2 {
		return nil, fmt.Errorf("bad input: %q", line)
	}

	var command command

	switch pieces[0] {
	case "forward":
		command.direction = forward
	case "up":
		command.direction = up
	case "down":
		command.direction = down
	default:
		return nil, fmt.Errorf("Unknown direction: %q", pieces[0])
	}

	command.quantity, err = strconv.Atoi(pieces[1])
	if err != nil {
		return nil, err
	}

	return &command, ioErr
}
